// RFC 6234 implementation
typedef unsigned char u8;
typedef long i32;
typedef unsigned int u32;
typedef long long i64;
typedef unsigned long long u64;
#define BYTES_TO_U32(b, i)                                                     \
    ((b)[(i)*4] << 24 | (b)[(i)*4 + 1] << 16 | (b)[(i)*4 + 2] << 8 |           \
     (b)[(i)*4 + 3])
#define U32_TO_BYTES(b, i, x)                                                  \
    (b)[(i)*4] = (x) >> 24;                                                    \
    (b)[(i)*4 + 1] = (x) >> 16;                                                \
    (b)[(i)*4 + 2] = (x) >> 8;                                                 \
    (b)[(i)*4 + 3] = (x);
#define __bswap_32(x)                                                          \
    ((((x)&0xff000000) >> 24) | (((x)&0x00ff0000) >> 8) |                      \
     (((x)&0x0000ff00) << 8) | (((x)&0x000000ff) << 24))

// Shift and rotate right
#define SHR(x, n) ((x & 0xFFFFFFFF) >> n)
#define ROTR(x, n) (SHR(x, n) | (x << (32 - n)))

// Choose
#define CH(x, y, z) (z ^ (x & (y ^ z)))
// Majority
#define MAJ(x, y, z) ((x & y) | (z & (x | y)))
// SIGMA 0 and 1
#define BSIG0(x) (ROTR(x, 2) ^ ROTR(x, 13) ^ ROTR(x, 22))
#define BSIG1(x) (ROTR(x, 6) ^ ROTR(x, 11) ^ ROTR(x, 25))
// sigma 0 and 1
#define SSIG0(x) (ROTR(x, 7) ^ ROTR(x, 18) ^ SHR(x, 3))
#define SSIG1(x) (ROTR(x, 17) ^ ROTR(x, 19) ^ SHR(x, 10))

#define sha256_step(a, b, c, d, e, f, g, h, Wt, K)                             \
    {                                                                          \
        T1 = h + BSIG1(e) + CH(e, f, g) + K + Wt;                              \
        T2 = BSIG0(a) + MAJ(a, b, c);                                          \
        d += T1;                                                               \
        h = T1 + T2;                                                           \
    }

#define fill_schedule(i)                                                       \
    (W[i] = SSIG1(W[i - 2]) + W[i - 7] + SSIG0(W[i - 15]) + W[i - 16])

inline void sha256_update(u32 *W, u32 *H) {
    u32 T1, T2;
    u32 a = H[0], b = H[1], c = H[2], d = H[3], e = H[4], f = H[5], g = H[6],
        h = H[7];

    // Message schedule W
    sha256_step(a, b, c, d, e, f, g, h, W[0], 0x428A2F98);
    sha256_step(h, a, b, c, d, e, f, g, W[1], 0x71374491);
    sha256_step(g, h, a, b, c, d, e, f, W[2], 0xB5C0FBCF);
    sha256_step(f, g, h, a, b, c, d, e, W[3], 0xE9B5DBA5);
    sha256_step(e, f, g, h, a, b, c, d, W[4], 0x3956C25B);
    sha256_step(d, e, f, g, h, a, b, c, W[5], 0x59F111F1);
    sha256_step(c, d, e, f, g, h, a, b, W[6], 0x923F82A4);
    sha256_step(b, c, d, e, f, g, h, a, W[7], 0xAB1C5ED5);
    sha256_step(a, b, c, d, e, f, g, h, W[8], 0xD807AA98);
    sha256_step(h, a, b, c, d, e, f, g, W[9], 0x12835B01);
    sha256_step(g, h, a, b, c, d, e, f, W[10], 0x243185BE);
    sha256_step(f, g, h, a, b, c, d, e, W[11], 0x550C7DC3);
    sha256_step(e, f, g, h, a, b, c, d, W[12], 0x72BE5D74);
    sha256_step(d, e, f, g, h, a, b, c, W[13], 0x80DEB1FE);
    sha256_step(c, d, e, f, g, h, a, b, W[14], 0x9BDC06A7);
    sha256_step(b, c, d, e, f, g, h, a, W[15], 0xC19BF174);
    sha256_step(a, b, c, d, e, f, g, h, fill_schedule(16), 0xE49B69C1);
    sha256_step(h, a, b, c, d, e, f, g, fill_schedule(17), 0xEFBE4786);
    sha256_step(g, h, a, b, c, d, e, f, fill_schedule(18), 0x0FC19DC6);
    sha256_step(f, g, h, a, b, c, d, e, fill_schedule(19), 0x240CA1CC);
    sha256_step(e, f, g, h, a, b, c, d, fill_schedule(20), 0x2DE92C6F);
    sha256_step(d, e, f, g, h, a, b, c, fill_schedule(21), 0x4A7484AA);
    sha256_step(c, d, e, f, g, h, a, b, fill_schedule(22), 0x5CB0A9DC);
    sha256_step(b, c, d, e, f, g, h, a, fill_schedule(23), 0x76F988DA);
    sha256_step(a, b, c, d, e, f, g, h, fill_schedule(24), 0x983E5152);
    sha256_step(h, a, b, c, d, e, f, g, fill_schedule(25), 0xA831C66D);
    sha256_step(g, h, a, b, c, d, e, f, fill_schedule(26), 0xB00327C8);
    sha256_step(f, g, h, a, b, c, d, e, fill_schedule(27), 0xBF597FC7);
    sha256_step(e, f, g, h, a, b, c, d, fill_schedule(28), 0xC6E00BF3);
    sha256_step(d, e, f, g, h, a, b, c, fill_schedule(29), 0xD5A79147);
    sha256_step(c, d, e, f, g, h, a, b, fill_schedule(30), 0x06CA6351);
    sha256_step(b, c, d, e, f, g, h, a, fill_schedule(31), 0x14292967);
    sha256_step(a, b, c, d, e, f, g, h, fill_schedule(32), 0x27B70A85);
    sha256_step(h, a, b, c, d, e, f, g, fill_schedule(33), 0x2E1B2138);
    sha256_step(g, h, a, b, c, d, e, f, fill_schedule(34), 0x4D2C6DFC);
    sha256_step(f, g, h, a, b, c, d, e, fill_schedule(35), 0x53380D13);
    sha256_step(e, f, g, h, a, b, c, d, fill_schedule(36), 0x650A7354);
    sha256_step(d, e, f, g, h, a, b, c, fill_schedule(37), 0x766A0ABB);
    sha256_step(c, d, e, f, g, h, a, b, fill_schedule(38), 0x81C2C92E);
    sha256_step(b, c, d, e, f, g, h, a, fill_schedule(39), 0x92722C85);
    sha256_step(a, b, c, d, e, f, g, h, fill_schedule(40), 0xA2BFE8A1);
    sha256_step(h, a, b, c, d, e, f, g, fill_schedule(41), 0xA81A664B);
    sha256_step(g, h, a, b, c, d, e, f, fill_schedule(42), 0xC24B8B70);
    sha256_step(f, g, h, a, b, c, d, e, fill_schedule(43), 0xC76C51A3);
    sha256_step(e, f, g, h, a, b, c, d, fill_schedule(44), 0xD192E819);
    sha256_step(d, e, f, g, h, a, b, c, fill_schedule(45), 0xD6990624);
    sha256_step(c, d, e, f, g, h, a, b, fill_schedule(46), 0xF40E3585);
    sha256_step(b, c, d, e, f, g, h, a, fill_schedule(47), 0x106AA070);
    sha256_step(a, b, c, d, e, f, g, h, fill_schedule(48), 0x19A4C116);
    sha256_step(h, a, b, c, d, e, f, g, fill_schedule(49), 0x1E376C08);
    sha256_step(g, h, a, b, c, d, e, f, fill_schedule(50), 0x2748774C);
    sha256_step(f, g, h, a, b, c, d, e, fill_schedule(51), 0x34B0BCB5);
    sha256_step(e, f, g, h, a, b, c, d, fill_schedule(52), 0x391C0CB3);
    sha256_step(d, e, f, g, h, a, b, c, fill_schedule(53), 0x4ED8AA4A);
    sha256_step(c, d, e, f, g, h, a, b, fill_schedule(54), 0x5B9CCA4F);
    sha256_step(b, c, d, e, f, g, h, a, fill_schedule(55), 0x682E6FF3);
    sha256_step(a, b, c, d, e, f, g, h, fill_schedule(56), 0x748F82EE);
    sha256_step(h, a, b, c, d, e, f, g, fill_schedule(57), 0x78A5636F);
    sha256_step(g, h, a, b, c, d, e, f, fill_schedule(58), 0x84C87814);
    sha256_step(f, g, h, a, b, c, d, e, fill_schedule(59), 0x8CC70208);
    sha256_step(e, f, g, h, a, b, c, d, fill_schedule(60), 0x90BEFFFA);
    sha256_step(d, e, f, g, h, a, b, c, fill_schedule(61), 0xA4506CEB);
    sha256_step(c, d, e, f, g, h, a, b, fill_schedule(62), 0xBEF9A3F7);
    sha256_step(b, c, d, e, f, g, h, a, fill_schedule(63), 0xC67178F2);

    H[0] += a;
    H[1] += b;
    H[2] += c;
    H[3] += d;
    H[4] += e;
    H[5] += f;
    H[6] += g;
    H[7] += h;
}

inline void memcpy(void *dest, const __global void *__private src, size_t len) {
    char *d = dest;
    const __global u8 *__private s = src;
    while (len--)
        *d++ = *s++;
}

inline void zero_mem(void *dest, size_t len) {
    char *d = dest;
    while (len--)
        *d++ = 0;
}

inline void swap_endianness(u32 *buff, size_t len) {
    while (len--) {
        *buff = __bswap_32(*buff);
        buff++;
    }
}

__kernel void sha256(__global const unsigned char *glob_data,
                     __global unsigned char *hashes) {
    size_t glob_id = get_global_id(0);
    i32 i = -1;
    u32 loc = 0;
    u8 str_len = 0;
    while (i < (i64)glob_id) {
        loc += str_len;
        str_len = glob_data[loc++];
        if (!str_len)
            return;
        i++;
    }

    const __global u8 *__private data = glob_data + loc;
    const __global u8 *__private end = data + str_len;
    u32 W[64] = {0};
    u32 H[8] = {
        0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
        0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19,
    };

    char carry_last_bit = 0;
    size_t len = 0;
    while ((len = end - data) > 55) {
        if (len > 64) {
            len = 64;
        } else if (len == 64) {
            carry_last_bit = 1;
        } else {
            zero_mem(((char *)W) + len + 1, 64 - len);
            ((u8 *)W)[len] = 0x80;
        }

        memcpy(W, data, len);
        swap_endianness(W, 16);
        sha256_update(W, H);
        data += len;
    }

    zero_mem(((char *)W) + len, 64 - len);
    memcpy(W, data, len);
    if (carry_last_bit || len > 0) {
        ((u8 *)W)[len] = 0x80;
    }

    u64 len_bits = str_len * 8;
    W[14] = len_bits >> 32;
    W[15] = len_bits;
    swap_endianness(W, 14);
    sha256_update(W, H);

    for (int i = 0; i < 8; i++) {
        U32_TO_BYTES(hashes + glob_id * 32, i, H[i]);
    }
}
