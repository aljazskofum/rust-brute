use std::ffi::c_void;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Write};
use std::mem::size_of;
use std::num::ParseIntError;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread::JoinHandle;
use std::time::Instant;
use std::{hint, ptr, thread};

// use mpi::traits::Communicator;

use opencl3::command_queue::CommandQueue;
use opencl3::context::Context;
use opencl3::device::{get_all_devices, Device, CL_DEVICE_TYPE_CPU, CL_DEVICE_TYPE_GPU};
use opencl3::kernel::{ExecuteKernel, Kernel};
use opencl3::memory::{Buffer, CL_MEM_READ_ONLY, CL_MEM_WRITE_ONLY};
use opencl3::program::Program;
use opencl3::types::{cl_uchar, CL_NON_BLOCKING};

use flate2::read::GzDecoder;

use crossbeam_queue::ArrayQueue;

use clap::Parser;

const PROGRAM_SOURCE: &str = include_str!("sha256.cl");
const KERNEL_NAME: &str = "sha256";
type HashInt = u64;
const INTEGER_SIZE: usize = size_of::<HashInt>();
const HASH_BLOCK_SIZE: usize = INTEGER_SIZE * 8;
const HASH_LEN: usize = HASH_BLOCK_SIZE / 2;
const PASSWORD_CAPACITY: usize = 32768;

#[derive(Parser, Debug)]
struct Cli {
    /// Lists OpenCL devices and exits
    #[arg(short, long, default_value_t = false)]
    list_devices: bool,
    /// Path to file with hashes to crack
    #[arg()]
    hashes: Option<String>,
    /// Path to wordlist (supports plaintext and GZ) or - for stdin
    #[arg(default_value_t = {"-".to_string()})]
    wordlist: String,
}

fn main() {
    let args: Cli = Cli::parse();
    let gpus: Vec<*mut c_void> = get_all_devices(CL_DEVICE_TYPE_GPU).unwrap_or_else(|err| {
        eprintln!("Could not get GPU devices: {err}");
        vec![]
    });
    let cpus: Vec<*mut c_void> = get_all_devices(CL_DEVICE_TYPE_CPU).unwrap_or_else(|err| {
        eprintln!("Could not get CPU devices: {err}");
        vec![]
    });

    if gpus.len() + cpus.len() == 0 {
        panic!("No devices!");
    }

    println!(
        "Found {} CPU worker(s) and {} GPU worker(s)",
        cpus.len(),
        gpus.len()
    );

    if args.list_devices {
        for device_id in cpus.iter().chain(gpus.iter()) {
            print_device_info(&Device::new(*device_id));
        }
        return;
    }

    // let (universe, _threading) =
    //     mpi::initialize_with_threading(mpi::Threading::Funneled).expect("Cannot init MPI");
    // let world = universe.world();
    // let size = world.size();
    // let rank = world.rank();
    // println!("MPI init, size={}, rank={}", size, rank);

    let password_queue: Arc<ArrayQueue<String>> = Arc::new(ArrayQueue::<String>::new(PASSWORD_CAPACITY));
    // Hash length is constant, so a 1D array is enough
    let mut hashes: Vec<u8> = vec![];

    match args.hashes {
        Some(path) => {
            let hashfile = File::open(path).expect("Cannot open hash file");
            let reader = BufReader::new(hashfile);
            for line in reader.lines() {
                if line.is_err() {
                    eprintln!("Hashes: {}", line.err().unwrap());
                    continue;
                }
                let line = line.unwrap();
                // Very important! We can get away with a 1D array of hashes because the hash length is constant!
                if line.len() != HASH_LEN * 2 {
                    eprintln!("{line} is not the correct length");
                    continue;
                }

                let line_hex: Result<Vec<u8>, ParseIntError> = (0..line.len())
                    .step_by(2)
                    .map(|i| u8::from_str_radix(&line[i..i + 2], 16))
                    .collect();
                if line_hex.is_err() {
                    let err = line_hex.err().unwrap();
                    eprintln!("Hashes: {} cannot be parsed {}", line, err);
                    continue;
                }
                hashes.extend(line_hex.unwrap());
            }
        },
        None => eprintln!("No hashes provided! This run will only be used as a benchmark")
    };

    let hashes = Arc::new(hashes);

    let run = Arc::new(AtomicBool::new(true));
    let mut threads = Vec::<JoinHandle<()>>::with_capacity(gpus.len() + cpus.len());

    // "Hyperthreading"
    println!("Thread group 1");
    create_threads(
        &mut threads,
        &password_queue,
        gpus.clone(),
        &run,
        PROGRAM_SOURCE,
        &hashes,
    );
    println!("Thread group 2");
    create_threads(
        &mut threads,
        &password_queue,
        gpus.clone(),
        &run,
        PROGRAM_SOURCE,
        &hashes,
    );
    println!("Thread group 3");
    create_threads(
        &mut threads,
        &password_queue,
        gpus.clone(),
        &run,
        PROGRAM_SOURCE,
        &hashes,
    );
    println!("Thread group 4");
    create_threads(
        &mut threads,
        &password_queue,
        gpus,
        &run,
        PROGRAM_SOURCE,
        &hashes,
    );
    // Can be a detriment to the GPU workload
    // create_threads(
    //     &mut threads,
    //     &password_queue,
    //     cpus,
    //     &run,
    //     PROGRAM_SOURCE,
    //     &hashes,
    // );

    let source: Box<dyn io::Read> = match args.wordlist.as_str() {
        "-" => Box::new(io::stdin()),
        _ => Box::new(File::open(args.wordlist).expect("Cannot open file")),
    };
    let mut reader = BufReader::new(source);
    let header = reader.fill_buf().expect("Could not read the file header");

    let mut lines_iter: Box<dyn Iterator<Item = Result<String, io::Error>>> =
        if header.len() > 1 && header[0] == 0x1f && header[1] == 0x8b {
            Box::new(BufReader::new(GzDecoder::new(reader)).lines())
        } else {
            Box::new(BufReader::new(reader).lines())
        };

    let start_time = Instant::now();
    while let Some(line) = lines_iter.next() {
        let line = match line {
            Ok(line) => line,
            Err(_) => continue, // Ignore decoding errors
        };
        while password_queue.is_full() {
            hint::spin_loop();
        }
        // Should never fail
        password_queue.push(line).unwrap();
    }

    run.store(false, Ordering::Relaxed);
    for thread in threads {
        match thread.join() {
            Ok(_) => (),
            Err(e) => eprintln!("A thread exited with {:#?}", e),
        }
    }

    println!("Total runtime: {:.3}s", start_time.elapsed().as_secs_f64());
}

fn create_threads(
    threads: &mut Vec<JoinHandle<()>>,
    password_queue: &Arc<ArrayQueue<String>>,
    devices: Vec<*mut c_void>,
    run: &Arc<AtomicBool>,
    program_source: &str,
    hashes: &Arc<Vec<u8>>,
) {
    let context = Arc::new(
        Context::from_devices(devices.as_slice(), &[], None, ptr::null_mut())
            .expect("Context::from_devices failed"),
    );

    let program = Arc::new(
        Program::create_and_build_from_source(&context, program_source, "").unwrap_or_else(|err| {
            panic!("Cannot create program: {err}");
        }),
    );

    for i in 0..devices.len() {
        let run = run.clone();
        let device = Device::new(devices[i]);
        let context = context.clone();
        let program = program.clone();
        let pass_queue = password_queue.clone();
        let hashes = hashes.clone();
        threads.push(
            thread::Builder::new()
                .name(format!("Worker {i}"))
                .spawn(move || gpu_thread(run, device, context, program, pass_queue, hashes))
                .unwrap(),
        );
    }
}

fn print_device_info(device: &Device) {
    println!(
        "Using device: {} ({:p})",
        device.name().unwrap_or("unknown".to_string()),
        device.id()
    );
}

fn gpu_thread(
    run: Arc<AtomicBool>,
    device: Device,
    context: Arc<Context>,
    program: Arc<Program>,
    password_queue: Arc<ArrayQueue<String>>,
    hashes: Arc<Vec<u8>>,
) {
    print_device_info(&device);
    let queue = unsafe {
        CommandQueue::create_with_properties(&context, device.id(), 0, 0)
            .expect("Cannot create command queue")
    };
    let kernel = Kernel::create(&program, KERNEL_NAME).expect("Cannot create kernel");

    // The input data
    let workers: usize = device.max_compute_units().unwrap_or_else(|err| {
        panic!("Could not get max work group size: {err}");
    }) as usize;
    let mut data_array = Vec::<u8>::with_capacity(workers * 10); // Average password length is about 8 so this should be enough for some time
    let mut result_vec = vec![0_u8; workers * HASH_LEN];

    while run.load(Ordering::Relaxed) || !password_queue.is_empty() {
        let mut items: usize = 0;
        data_array.clear();
        while !password_queue.is_empty() {
            let line = match password_queue.pop() {
                Some(str) => str, // Transfer ownership
                None => {
                    hint::spin_loop();
                    continue;
                }
            };
            let line = line.as_bytes();

            if line.len() > 255 {
                cold_dummy();
                continue;
            }
            data_array.push(line.len() as u8);
            data_array.extend(line);

            items += 1;
            // We can only have so many items each work cycle
            if items == workers {
                cold_dummy();
                break;
            }
        }

        if items == 0 {
            cold_dummy();
            continue;
        }

        // Terminate the array
        data_array.push(0_u8);

        let mut cl_data_buffer = unsafe {
            Buffer::<cl_uchar>::create(
                &context,
                CL_MEM_READ_ONLY,
                data_array.len(),
                ptr::null_mut(),
            )
            .unwrap()
        };

        let result_slice = &mut result_vec[..items * HASH_LEN];
        let cl_hashes_out = unsafe {
            Buffer::<cl_uchar>::create(
                &context,
                CL_MEM_WRITE_ONLY,
                result_slice.len(),
                ptr::null_mut(),
            )
            .unwrap()
        };

        // Non-blocking write
        let data_event = unsafe {
            queue
                .enqueue_write_buffer(
                    &mut cl_data_buffer,
                    CL_NON_BLOCKING,
                    0,
                    &data_array.as_slice(),
                    &[],
                )
                .unwrap()
        };

        let kernel_event = unsafe {
            ExecuteKernel::new(&kernel)
                .set_arg(&cl_data_buffer)
                .set_arg(&cl_hashes_out)
                .set_global_work_size(workers)
                .set_wait_event(&data_event)
                .enqueue_nd_range(&queue)
                .unwrap_or_else(|err| {
                    panic!("Execute kernel error: {err}");
                })
        };

        // Create a results array to hold the results from the OpenCL device
        // and enqueue a read command to read the device buffer into the array
        // after the kernel event completes.
        let read_event = unsafe {
            queue
                .enqueue_read_buffer(
                    &cl_hashes_out,
                    CL_NON_BLOCKING,
                    0,
                    result_slice,
                    &[kernel_event.get()],
                )
                .unwrap_or_else(|err| {
                    panic!("Could not enqueue read buffer: {err}");
                })
        };

        // We can be pretty certain the kernel won't complete this timeslice,
        // so yield whatever remaining time we have
        thread::yield_now();
        // Wait for the read_event to complete.
        read_event.wait().unwrap();

        for hash in hashes.chunks_exact(32) {
            let res = result_slice
                .chunks_exact(32)
                .position(|a| a.iter().zip(hash).all(|(a, b)| *a == *b));
            if let Some(res) = res {
                let stdout = io::stdout();
                let mut handle = stdout.lock();
                write!(&mut handle, "Match! ").unwrap();
                for b in hash {
                    write!(&mut handle, "{:02x}", *b).unwrap();
                }
                write!(&mut handle, ":").unwrap();
                let mut i: i32 = -1;
                let mut loc: usize = 0;
                let mut str_len: usize = 0;
                while i < res as i32 {
                    loc += str_len;
                    str_len = data_array[loc] as usize;
                    loc += 1;
                    i += 1;
                }
                for b in &data_array[loc..loc + str_len] {
                    write!(&mut handle, "{}", char::from(*b)).unwrap();
                }
                writeln!(&mut handle).unwrap();
            }
        }
    }
}

#[cold]
#[inline]
fn cold_dummy() {}
