# module load Rust  # We need a specific version of rust nightly, this won't work
module purge
module load CUDA
module load mpi
cd cpu
srun -G 1 cargo build --release
