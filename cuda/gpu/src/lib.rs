#![cfg_attr(
    target_os = "cuda",
    no_std,
    feature(register_attr),
    register_attr(nvvm_internal)
)]

use cuda_std::*;

#[inline(always)]
fn ch(x: u32, y: u32, z: u32) -> u32 {
    z ^ (x & (y ^ z))
}

#[inline(always)]
fn maj(x: u32, y: u32, z: u32) -> u32 {
    (x & y) | (z & (x | y))
}

#[inline(always)]
#[allow(non_snake_case)]
fn Sig0(x: u32) -> u32 {
    x.rotate_right(2) ^ x.rotate_right(13) ^ x.rotate_right(22)
}

#[inline(always)]
#[allow(non_snake_case)]
fn Sig1(x: u32) -> u32 {
    x.rotate_right(6) ^ x.rotate_right(11) ^ x.rotate_right(25)
}

#[inline(always)]
fn sig0(x: u32) -> u32 {
    x.rotate_right(7) ^ x.rotate_right(18) ^ (x >> 3)
}

#[inline(always)]
fn sig1(x: u32) -> u32 {
    x.rotate_right(17) ^ x.rotate_right(19) ^ (x >> 10)
}

macro_rules! sha256step {
    ($A:expr, $B:expr, $C:expr, $D:expr, $E:expr, $F:expr, $G:expr, $H:expr, $Wt:expr, $K:expr) => {{
        let temp1 = $H + Sig1($E) + ch($E, $F, $G) + $K + $Wt;
        let temp2 = Sig0($A) + maj($A, $B, $C);
        $D += temp1;
        $H = temp1 + temp2;
    }};
}

macro_rules! fill_schedule {
    ($Wt:expr, $i:expr) => {{
        $Wt[$i] = sig1(($Wt[$i - 2])) + $Wt[$i - 7] + sig0($Wt[$i - 15]) + $Wt[$i - 16];
        $Wt[$i]
    }};
}

#[allow(non_snake_case)]
fn sha256_update(W: &mut [u32; 64], ctx: &mut [u32; 8]) {
    let (mut A, mut B, mut C, mut D): (u32, u32, u32, u32);
    let (mut E, mut F, mut G, mut H): (u32, u32, u32, u32);

    A = ctx[0];
    B = ctx[1];
    C = ctx[2];
    D = ctx[3];
    E = ctx[4];
    F = ctx[5];
    G = ctx[6];
    H = ctx[7];

    sha256step!(A, B, C, D, E, F, G, H, W[0], 0x428A2F98);
    sha256step!(H, A, B, C, D, E, F, G, W[1], 0x71374491);
    sha256step!(G, H, A, B, C, D, E, F, W[2], 0xB5C0FBCF);
    sha256step!(F, G, H, A, B, C, D, E, W[3], 0xE9B5DBA5);
    sha256step!(E, F, G, H, A, B, C, D, W[4], 0x3956C25B);
    sha256step!(D, E, F, G, H, A, B, C, W[5], 0x59F111F1);
    sha256step!(C, D, E, F, G, H, A, B, W[6], 0x923F82A4);
    sha256step!(B, C, D, E, F, G, H, A, W[7], 0xAB1C5ED5);
    sha256step!(A, B, C, D, E, F, G, H, W[8], 0xD807AA98);
    sha256step!(H, A, B, C, D, E, F, G, W[9], 0x12835B01);
    sha256step!(G, H, A, B, C, D, E, F, W[10], 0x243185BE);
    sha256step!(F, G, H, A, B, C, D, E, W[11], 0x550C7DC3);
    sha256step!(E, F, G, H, A, B, C, D, W[12], 0x72BE5D74);
    sha256step!(D, E, F, G, H, A, B, C, W[13], 0x80DEB1FE);
    sha256step!(C, D, E, F, G, H, A, B, W[14], 0x9BDC06A7);
    sha256step!(B, C, D, E, F, G, H, A, W[15], 0xC19BF174);
    sha256step!(A, B, C, D, E, F, G, H, fill_schedule!(W, 16), 0xE49B69C1);
    sha256step!(H, A, B, C, D, E, F, G, fill_schedule!(W, 17), 0xEFBE4786);
    sha256step!(G, H, A, B, C, D, E, F, fill_schedule!(W, 18), 0x0FC19DC6);
    sha256step!(F, G, H, A, B, C, D, E, fill_schedule!(W, 19), 0x240CA1CC);
    sha256step!(E, F, G, H, A, B, C, D, fill_schedule!(W, 20), 0x2DE92C6F);
    sha256step!(D, E, F, G, H, A, B, C, fill_schedule!(W, 21), 0x4A7484AA);
    sha256step!(C, D, E, F, G, H, A, B, fill_schedule!(W, 22), 0x5CB0A9DC);
    sha256step!(B, C, D, E, F, G, H, A, fill_schedule!(W, 23), 0x76F988DA);
    sha256step!(A, B, C, D, E, F, G, H, fill_schedule!(W, 24), 0x983E5152);
    sha256step!(H, A, B, C, D, E, F, G, fill_schedule!(W, 25), 0xA831C66D);
    sha256step!(G, H, A, B, C, D, E, F, fill_schedule!(W, 26), 0xB00327C8);
    sha256step!(F, G, H, A, B, C, D, E, fill_schedule!(W, 27), 0xBF597FC7);
    sha256step!(E, F, G, H, A, B, C, D, fill_schedule!(W, 28), 0xC6E00BF3);
    sha256step!(D, E, F, G, H, A, B, C, fill_schedule!(W, 29), 0xD5A79147);
    sha256step!(C, D, E, F, G, H, A, B, fill_schedule!(W, 30), 0x06CA6351);
    sha256step!(B, C, D, E, F, G, H, A, fill_schedule!(W, 31), 0x14292967);
    sha256step!(A, B, C, D, E, F, G, H, fill_schedule!(W, 32), 0x27B70A85);
    sha256step!(H, A, B, C, D, E, F, G, fill_schedule!(W, 33), 0x2E1B2138);
    sha256step!(G, H, A, B, C, D, E, F, fill_schedule!(W, 34), 0x4D2C6DFC);
    sha256step!(F, G, H, A, B, C, D, E, fill_schedule!(W, 35), 0x53380D13);
    sha256step!(E, F, G, H, A, B, C, D, fill_schedule!(W, 36), 0x650A7354);
    sha256step!(D, E, F, G, H, A, B, C, fill_schedule!(W, 37), 0x766A0ABB);
    sha256step!(C, D, E, F, G, H, A, B, fill_schedule!(W, 38), 0x81C2C92E);
    sha256step!(B, C, D, E, F, G, H, A, fill_schedule!(W, 39), 0x92722C85);
    sha256step!(A, B, C, D, E, F, G, H, fill_schedule!(W, 40), 0xA2BFE8A1);
    sha256step!(H, A, B, C, D, E, F, G, fill_schedule!(W, 41), 0xA81A664B);
    sha256step!(G, H, A, B, C, D, E, F, fill_schedule!(W, 42), 0xC24B8B70);
    sha256step!(F, G, H, A, B, C, D, E, fill_schedule!(W, 43), 0xC76C51A3);
    sha256step!(E, F, G, H, A, B, C, D, fill_schedule!(W, 44), 0xD192E819);
    sha256step!(D, E, F, G, H, A, B, C, fill_schedule!(W, 45), 0xD6990624);
    sha256step!(C, D, E, F, G, H, A, B, fill_schedule!(W, 46), 0xF40E3585);
    sha256step!(B, C, D, E, F, G, H, A, fill_schedule!(W, 47), 0x106AA070);
    sha256step!(A, B, C, D, E, F, G, H, fill_schedule!(W, 48), 0x19A4C116);
    sha256step!(H, A, B, C, D, E, F, G, fill_schedule!(W, 49), 0x1E376C08);
    sha256step!(G, H, A, B, C, D, E, F, fill_schedule!(W, 50), 0x2748774C);
    sha256step!(F, G, H, A, B, C, D, E, fill_schedule!(W, 51), 0x34B0BCB5);
    sha256step!(E, F, G, H, A, B, C, D, fill_schedule!(W, 52), 0x391C0CB3);
    sha256step!(D, E, F, G, H, A, B, C, fill_schedule!(W, 53), 0x4ED8AA4A);
    sha256step!(C, D, E, F, G, H, A, B, fill_schedule!(W, 54), 0x5B9CCA4F);
    sha256step!(B, C, D, E, F, G, H, A, fill_schedule!(W, 55), 0x682E6FF3);
    sha256step!(A, B, C, D, E, F, G, H, fill_schedule!(W, 56), 0x748F82EE);
    sha256step!(H, A, B, C, D, E, F, G, fill_schedule!(W, 57), 0x78A5636F);
    sha256step!(G, H, A, B, C, D, E, F, fill_schedule!(W, 58), 0x84C87814);
    sha256step!(F, G, H, A, B, C, D, E, fill_schedule!(W, 59), 0x8CC70208);
    sha256step!(E, F, G, H, A, B, C, D, fill_schedule!(W, 60), 0x90BEFFFA);
    sha256step!(D, E, F, G, H, A, B, C, fill_schedule!(W, 61), 0xA4506CEB);
    sha256step!(C, D, E, F, G, H, A, B, fill_schedule!(W, 62), 0xBEF9A3F7);
    sha256step!(B, C, D, E, F, G, H, A, fill_schedule!(W, 63), 0xC67178F2);

    ctx[0] += A;
    ctx[1] += B;
    ctx[2] += C;
    ctx[3] += D;
    ctx[4] += E;
    ctx[5] += F;
    ctx[6] += G;
    ctx[7] += H;
}

#[kernel]
pub unsafe fn sha256(data: *const u8, hashes: *mut u8) {
    let glob_id: u32 = thread::index_1d(); // add a length arg to check for overflows? run with the test
    sha256_internal(glob_id, data, hashes);
}

#[allow(non_snake_case)]
fn sha256_internal(glob_id: u32, data: *const u8, hashes: *mut u8) {
    let mut i: i32 = -1;
    let mut loc: u32 = 0;
    let mut str_len: u8 = 0;
    // data structure:
    //   - 1 byte: string length
    //   - string length bytes: string
    // The max length could be increased, but this would make PCI transfers slower
    while i < glob_id as i32 {
        loc += str_len as u32;
        str_len = unsafe { data.add(loc as usize).read() };
        if str_len == 0 {
            return;
        }
        loc += 1;
        i += 1;
    }

    let mut data = unsafe { core::slice::from_raw_parts(data.add(loc as usize), str_len as usize) };
    let mut W: [u32; 64] = [0; 64];
    let mut H: [u32; 8] = [
        0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab,
        0x5be0cd19,
    ];

    let mut carry_last_bit = false;
    // If data.len() > 55, then data does not fit within one block.
    // The last 8 bytes need to be reserved for length, a u64
    while data.len() > 55 {
        let aligned = unsafe { &mut W.align_to_mut::<u8>().1[..64] };
        let part;
        if data.len() > 64 {
            part = &data[..64];
            data = &data[64..];
        } else {
            part = data;
            if data.len() < 64 {
                aligned[data.len()] = 0x80;
                aligned[data.len()+1..].fill(0);
            } else {
                // data.len() == 64
                carry_last_bit = true;
            }
            data = &[];
        }

        aligned[..part.len()].copy_from_slice(part);
        for w in W.iter_mut().take(16) {
            *w = w.to_be();
        }
        sha256_update(&mut W, &mut H);
    }

    // Last block
    let aligned = unsafe { W.align_to_mut::<u8>().1 };
    aligned[data.len()..56].fill(0);
    aligned[..data.len()].copy_from_slice(data);
    if carry_last_bit || data.len() > 0 {
        aligned[data.len()] = 0x80;
    }

    // This line can fail tests, but works in prod
    // https://doc.rust-lang.org/std/primitive.slice.html#method.align_to
    // W[14..16].align_to_mut::<u64>().1[0] = (str_len as u64 * 8).to_be(); // does not work
    // W[14..16].align_to_mut::<u64>().1[0] = ((cumulative_len + data.len()) as u64 * 8).to_be(); // works

    let len_bits = str_len as u64 * 8;
    W[14] = (len_bits >> 32) as u32;
    W[15] = len_bits as u32;
    for w in W.iter_mut().take(14) {
        *w = w.to_be();
    }
    sha256_update(&mut W, &mut H);
    for h in H.iter_mut() {
        *h = h.to_be();
    }

    unsafe {
        hashes
            .add(glob_id as usize * 32)
            .copy_from(H.align_to::<u8>().1.as_ptr(), 32);
    }
}

#[cfg(test)]
mod tests {
    use crate::sha256_internal;
    use std::fmt::Write;

    macro_rules! prepare_str {
        ( $( $x:expr ),* ) => {
            {
                let mut data = Vec::new();
                $(
                    assert!($x.len() < 256);
                    data.push($x.len() as u8);
                    data.extend($x);
                )*
                data.push(0_u8);
                data
            }
        };
    }

    fn encode_hex(bytes: &[u8]) -> String {
        let mut s = String::with_capacity(bytes.len() * 2);
        for b in bytes {
            write!(&mut s, "{:02x}", *b).unwrap();
        }
        s
    }

    #[test]
    fn simple() {
        let data = prepare_str!(b"123456");
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(0, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92"
        );
    }

    #[test]
    fn emoji() {
        let data = prepare_str!(b"\xF0\x9F\x91\x8D"); // 👍
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(0, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "5d57d39e6b7d8ea050980e6665bb1da357db14249d74e122f7d4e8cc2253beff"
        );
    }

    #[test]
    fn awkward_chunk_one() {
        let data = prepare_str!(b"This string will split awkwardly as it's just over 55 b long");
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(0, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "b597edaf5c527a0ea62ab1107bc68c2d4108ff9b358dc912367a2bd9d93c0e96"
        );
    }

    #[test]
    fn awkward_chunk_two() {
        let data =
            prepare_str!(b"I take up exactly 64 bytes, or one block of data. I may break!  ");
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(0, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "e9f3528c005484688ced86c4530beca70e592a609ee89f372693d10c583b9615"
        );
    }

    #[test]
    fn long() {
        let data =
            prepare_str!(b"Hello, this is a long test string. It should take up multiple blocks");
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(0, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "ed3b60002663f5a85ba28fd4dbcf83cbf390cc6a83e6fd6a3f450651fc7b09e6"
        );
    }

    #[test]
    fn very_long() {
        let data =
            prepare_str!(b"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in \
                          venenatis eros. Mauris faucibus non nisl sit amet faucibus. Aliquam nec \
                          tellus eget nibh volutpat tempor in non purus. Integer eget erat non lectus \
                          congue pulvinar pretium a eros.");
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(0, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "f5fc07b3e01b064fccfb53cdf9d6e4f23b17b636a841f95dce3503fb30ba2d8d"
        );
    }

    #[test]
    fn multiple_blocks() {
        let data =
            prepare_str!(b"String1", b"String2", b"String3");
        let mut result: [u8; 96] = [0; 96];
        sha256_internal(0, data.as_ptr(), result.as_mut_ptr());
        sha256_internal(1, data.as_ptr(), result.as_mut_ptr());
        sha256_internal(2, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result[0..32]),
            "ca5b6ff1563ff21dda1ae9e4b646b8afc47a3a42e0ec5c2872e1425f60daa731"
        );
        assert_eq!(
            encode_hex(&result[32..64]),
            "f76684b352c775ab54ea625541f8c369bcab10b5f01b6389683c05bb01556b2c"
        );
        assert_eq!(
            encode_hex(&result[64..]),
            "2e81041bbc6c19411cde57d1578ccf97f29ce37dc0e5fb595fec151177e840b3"
        );
    }

    #[test]
    fn empty_one() {
        let data =
            prepare_str!(b"");
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(100, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "0000000000000000000000000000000000000000000000000000000000000000"
        );
    }

    #[test]
    fn empty_two() {
        let data =
            prepare_str!(b"there is", b"data stored", b"in this vec");
        let mut result: [u8; 32] = [0; 32];
        sha256_internal(5, data.as_ptr(), result.as_mut_ptr());
        assert_eq!(
            encode_hex(&result),
            "0000000000000000000000000000000000000000000000000000000000000000"
        );
    }
}
