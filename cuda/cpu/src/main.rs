use std::fs::File;
use std::io::{self, BufRead, BufReader, Write};
use std::mem::size_of;
use std::num::ParseIntError;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread::JoinHandle;
use std::time::Instant;
use std::{hint, thread};

use cust::context::CurrentContext;
use cust::{self, launch, prelude::*, CudaFlags};

use flate2::read::GzDecoder;

use crossbeam_queue::ArrayQueue;

use mpi::topology::SystemCommunicator;
use mpi::traits::{AsDatatype, Communicator, Destination, Source};
use mpi::datatype::Equivalence;

use clap::Parser;

const PTX_COMPILED: &str = include_str!("../target/sha256.ptx");
const PTX_FUNC: &str = "sha256";
type HashInt = u64;
const INTEGER_SIZE: usize = size_of::<HashInt>();
const HASH_BLOCK_SIZE: usize = INTEGER_SIZE * 8;
const HASH_LEN: usize = HASH_BLOCK_SIZE / 2;
const PASSWORD_CAPACITY: usize = 1 << 16;
// const MPI_BUFFER_SIZE: usize = PASSWORD_CAPACITY << 4;

#[derive(Parser, Debug)]
struct Cli {
    /// Lists CUDA devices and exits
    #[clap(short, long, action, default_value_t = false)]
    list_devices: bool,
    /// Path to file with hashes to crack
    #[clap()]
    hashes: Option<String>,
    /// Path to wordlist (supports plaintext and GZ) or - for stdin
    #[clap(default_value_t = {"-".to_string()})]
    wordlist: String,
}

fn main() {
    let args: Cli = Cli::parse();
    cust::init(CudaFlags::empty()).expect("Cannot init CUDA. Do you have a CUDA capable GPU?");
    let num_devices =
        Device::num_devices().expect("Cannot get the number of CUDA devices. Is CUDA installed?");
    if num_devices == 0 {
        panic!("No CUDA capable devices detected on the system!");
    }

    if args.list_devices {
        for i in 0..num_devices {
            let device = Device::get_device(i).unwrap();
            let context = Context::new(device).unwrap();
            print_device_info(&context, &device, 0);
        }
        return;
    }

    let (universe, _threading) =
        mpi::initialize_with_threading(mpi::Threading::Funneled).expect("Cannot init MPI");
    // universe.set_buffer_size(MPI_BUFFER_SIZE);
    let world = universe.world();
    let size = world.size();
    let rank = world.rank();
    println!("MPI init, size={}, rank={}", size, rank);

    println!("({}) Found {} CUDA capable GPU(s)", rank, num_devices);

    let password_queue: Arc<ArrayQueue<String>> = Arc::new(ArrayQueue::<String>::new(PASSWORD_CAPACITY));
    let mut hashes: Vec<u8> = vec![];

    match args.hashes {
        Some(path) => {
            let hashfile = File::open(path).expect("Cannot open hash file");
            let reader = BufReader::new(hashfile);
            for line in reader.lines() {
                if line.is_err() {
                    eprintln!("Hashes: {}", line.err().unwrap());
                    continue;
                }
                let line = line.unwrap();
                // Very important! We can get away with a 1D array of hashes because the hash length is constant!
                if line.len() != HASH_LEN * 2 {
                    eprintln!("{line} is not the correct length");
                    continue;
                }

                let line_hex: Result<Vec<u8>, ParseIntError> = (0..line.len())
                    .step_by(2)
                    .map(|i| u8::from_str_radix(&line[i..i + 2], 16))
                    .collect();
                if line_hex.is_err() {
                    let err = line_hex.err().unwrap();
                    eprintln!("Hashes: {} cannot be parsed {}", line, err);
                    continue;
                }
                hashes.extend(line_hex.unwrap());
            }
        }
        None => eprintln!("No hashes provided! This run will only be used as a benchmark"),
    };

    let hashes = Arc::new(hashes);
    let run = Arc::new(AtomicBool::new(true));
    let mut threads = Vec::<JoinHandle<()>>::with_capacity(num_devices as usize);

    // "Hyperthreading"
    println!("({}) Thread group 1", rank);
    create_threads(&mut threads, &password_queue, num_devices, &run, &hashes, rank);
    println!("({}) Thread group 2", rank);
    create_threads(&mut threads, &password_queue, num_devices, &run, &hashes, rank);

    let start_time = Instant::now();
    if rank == 0 {
        producer(run, args.wordlist, password_queue, world);
    } else {
        consumer(run, password_queue, world);
    }

    for thread in threads {
        match thread.join() {
            Ok(_) => (),
            Err(e) => eprintln!("A thread exited with {:#?}", e),
        }
    }

    println!(
        "Total runtime ({}): {:.3}s",
        rank,
        start_time.elapsed().as_secs_f64()
    );
}

fn producer(
    run: Arc<AtomicBool>,
    wordlist: String,
    password_queue: Arc<ArrayQueue<String>>,
    world: SystemCommunicator,
) {
    let world_size = world.size();
    let source: Box<dyn io::Read> = match wordlist.as_str() {
        "-" => Box::new(io::stdin()),
        _ => Box::new(File::open(wordlist).expect("Cannot open file")),
    };
    let mut reader = BufReader::new(source);
    let header = reader.fill_buf().expect("Could not read the file header");

    let mut lines_iter: Box<dyn Iterator<Item = Result<String, io::Error>>> =
        if header.len() > 1 && header[0] == 0x1f && header[1] == 0x8b {
            Box::new(BufReader::new(GzDecoder::new(reader)).lines())
        } else {
            Box::new(BufReader::new(reader).lines())
        };

    let mut sending_to = 0;
    'outer: while let Some(line) = lines_iter.next() {
        let line = match line {
            Ok(line) => line,
            Err(_) => continue, // Ignore decoding errors
        };

        if line.len() == 0 || line.len() > 255 {
            cold_dummy();
            continue;
        }

        while sending_to == 0 {
            sending_to = (sending_to + 1) % world_size;
            if !password_queue.is_full() {
                password_queue.push(line).unwrap();
                continue 'outer;
            }
            hint::spin_loop();
        }

        let line = line.as_bytes();
        // world.process_at_rank(sending_to).buffered_send(&line[..]);
        world.process_at_rank(sending_to).send(&line[..]);
        sending_to = (sending_to + 1) % world_size;
    }

    for i in 1..world_size {
        println!("Terminating {}", i);
        world.process_at_rank(i).send(&[0_u8]);
    }
    run.store(false, Ordering::Relaxed);
    println!("Read completed");
}

fn consumer(
    run: Arc<AtomicBool>,
    password_queue: Arc<ArrayQueue<String>>,
    world: SystemCommunicator,
) {
    let mut buffer = vec![0_u8; 255];
    loop {
        let (message, status) = world.process_at_rank(0).matched_probe();
        let len = status.count(buffer.as_datatype());
        if len > buffer.len() as i32 {
            cold_dummy();
            buffer.reserve((len - buffer.len() as i32) as usize);
        }
        message.matched_receive_into(&mut buffer);
        if len == 1 && buffer[0] == 0 {
            cold_dummy();
            break;
        }
        while password_queue.is_full() {
            cold_dummy();
            hint::spin_loop();
        }
        password_queue
            .push(String::from_utf8(buffer[..len as usize].to_vec()).unwrap())
            .unwrap();
    }

    run.store(false, Ordering::Relaxed);
    println!("Process {} exiting", world.rank());
}

fn print_device_info(context: &Context, device: &Device, rank: i32) {
    let cuda_version = context.get_api_version();
    let version = match cuda_version {
        Ok(version) => format!("v{}.{}", version.major(), version.minor()),
        Err(_) => "<unknown>".to_string(),
    };

    println!(
        "({}) Using API {} on device: {} ({})",
        rank,
        version,
        device.name().unwrap_or("unknown".to_string()),
        device.as_raw()
    );
}

fn create_threads(
    threads: &mut Vec<JoinHandle<()>>,
    password_queue: &Arc<ArrayQueue<String>>,
    num_devices: u32,
    run: &Arc<AtomicBool>,
    hashes: &Arc<Vec<u8>>,
    rank: i32,
) {
    for device_number in 0..num_devices {
        let run = run.clone();
        let password_queue = password_queue.clone();
        let hashes = hashes.clone();
        threads.push(
            thread::Builder::new()
                .name(format!("GPU worker {device_number}"))
                .spawn(move || gpu_thread(run, device_number, password_queue, hashes, rank))
                .unwrap(),
        );
    }
}

fn gpu_thread(
    run: Arc<AtomicBool>,
    device_number: u32,
    password_queue: Arc<ArrayQueue<String>>,
    hashes: Arc<Vec<u8>>,
    rank: i32,
) {
    let device = Device::get_device(device_number)
        .expect(format!("Could not contruct device {}", device_number).as_str());
    let ctx = Context::new(device).expect("Could not create context");
    ctx.set_flags(ContextFlags::SCHED_AUTO)
        .expect("Sched auto failed");
    print_device_info(&ctx, &device, rank);

    let module = Module::from_ptx(PTX_COMPILED, &[]).expect("Could not load PTX module");
    let sha256 = module.get_function(PTX_FUNC).unwrap();
    let stream = Stream::new(
        StreamFlags::NON_BLOCKING,
        Some(
            CurrentContext::get_stream_priority_range()
                .expect("Could not get priorities")
                .greatest,
        ),
    )
    .expect("Stream could not be created");
    let (grid_size, block_size) = sha256
        .suggested_launch_configuration(0, 0.into())
        .expect("Cannot get suggested launch configuration");

    let workers: usize = (block_size * grid_size) as usize;
    let mut data_array = Vec::<u8>::with_capacity(workers * 10); // Average password length is about 8 so this should be enough for some time
    let mut result_array = vec![0_u8; workers * HASH_LEN];

    while run.load(Ordering::Relaxed) || !password_queue.is_empty() {
        let mut items: usize = 0;
        data_array.clear();
        while !password_queue.is_empty() {
            let line = match password_queue.pop() {
                Some(str) => str, // Transfer ownership
                None => {
                    hint::spin_loop();
                    continue;
                }
            };
            // println!("{}", &line);
            let line = line.as_bytes();

            if line.len() > 255 {
                cold_dummy();
                continue;
            }
            data_array.push(line.len() as u8);
            data_array.extend(line);

            items += 1;
            // We can only have so many items each work cycle
            if items == workers {
                cold_dummy();
                break;
            }
        }

        if items == 0 {
            cold_dummy();
            continue;
        }

        // Terminate the array
        data_array.push(0_u8);

        let passwords_gpu = data_array
            .as_slice()
            .as_dbuf()
            .expect("Could not create input buffer");
        let result_gpu = (&result_array[..items * HASH_LEN])
            .as_dbuf()
            .expect("Could not create result buffer");

        unsafe {
            launch!(
                sha256<<<grid_size, block_size, 0, stream>>>(
                    passwords_gpu.as_device_ptr(),
                    result_gpu.as_device_ptr(),
                )
            )
            .unwrap();
        }

        stream.synchronize().unwrap();
        result_gpu
            .copy_to(&mut result_array[..items * HASH_LEN])
            .unwrap();

        'outer: for hash in hashes.chunks_exact(32) {
            let res = result_array
                .as_slice()
                .chunks_exact(32)
                .position(|a| a.iter().zip(hash).all(|(a, b)| *a == *b));
            if let Some(res) = res {
                let mut i: i32 = -1;
                let mut loc: usize = 0;
                let mut str_len: usize = 0;
                while i < res as i32 {
                    loc += str_len;
                    if loc >= data_array.len() {
                        // println!("l:{} s:{}; {:?}", loc, str_len, &data_array[loc-20..]);
                    }
                    str_len = data_array[loc] as usize;
                    // You think this wouldn't be necessary, but for some dumb reason
                    // when used in conjunction with MPI it just isn't here
                    if str_len == 0 {
                        // writeln!(&mut handle, "<UNKNOWN>").unwrap();
                        break 'outer;
                    }
                    loc += 1;
                    i += 1;
                }
                let stdout = io::stdout();
                let mut handle = stdout.lock();
                write!(&mut handle, "Match! ").unwrap();
                for b in hash {
                    write!(&mut handle, "{:02x}", *b).unwrap();
                }
                write!(&mut handle, ":").unwrap();
                for b in &data_array[loc..loc + str_len] {
                    write!(&mut handle, "{}", char::from(*b)).unwrap();
                }
                writeln!(&mut handle).unwrap();
            }
        }
    }
}

#[cold]
#[inline]
fn cold_dummy() {}
