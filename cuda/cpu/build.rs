use cuda_builder::CudaBuilder;

fn main() {
    CudaBuilder::new("../gpu")
        .copy_to("target/sha256.ptx")
        .build()
        .unwrap();
}
